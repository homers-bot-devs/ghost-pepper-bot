const config = require('../config')
const { MessageEmbed } = require("discord.js");

exports.run = async(client, message, args) => {
  let embed = new MessageEmbed()
  .setTitle("The creator")
  .setDescription("learn who wrote the original code for this bot")
  .addField("About the creator","Reyansh-Khobragade wrote rey-bot-v2 (now reyzor) which this bot is based of")
  .addField("His GitHub","https://github.com/Reyansh-Khobragade")
  .addField("The original bot with code","https://github.com/Reyansh-Khobragade/rey-bot-v2")
         .setColor("BLUE")
         .setTimestamp()
         .setFooter(`${message.author.tag}`, message.author.displayAvatarURL())
    message.channel.send(embed)
}
