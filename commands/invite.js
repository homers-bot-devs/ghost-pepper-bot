const { MessageEmbed } = require("discord.js");

exports.run = async(client, message, args) => {
  let embed = new MessageEmbed()
  .setTitle("Invite me")
  .setDescription("Invite me to your server if you want to")
  .addField("Invite Link","https://discord.com/oauth2/authorize?client_id=841413638497501254&permissions=8&scope=bot")
         .setColor("GREEN")
         .setTimestamp()
         .setFooter(`${message.author.tag}`, message.author.displayAvatarURL())
    message.channel.send(embed)
}
